#!/bin/bash

UPTIME=10
TG_BOT_TOKEN="XXXXXXXXXXXXXXXXXXXXX"
CHAT_ID="-4121164297"
TG_API_URL="https://api.telegram.org/bot$TG_BOT_TOKEN/sendMessage"
MESSAGE_TEXT="School21%20CI/CD%20[by%20willumye]%0A----------%0AProject:%20$CI_PROJECT_NAME%0AGitlab%20branch:%20$CI_COMMIT_REF_SLUG%0AURL:%20$CI_PROJECT_URL/pipelines/$CI_PIPELINE_ID/%0A%0ADeployment%20status:%20$1%20$2"
MESSAGE_URL_POST="chat_id=$CHAT_ID&disable_web_page_preview=1&text=$MESSAGE_TEXT"

# curl - утилита передачи данных с помощью URL
# -s - silent
# --max-time - время выполнения команды
# -d - режим отправки запроса POST на URL
echo "$MESSAGE_TEXT\n"
curl -s --max-time $UPTIME -d $MESSAGE_URL_POST $TG_API_URL > /dev/null
