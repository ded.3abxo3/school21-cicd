#!/bin/bash

UPTIME=10
TG_BOT_TOKEN="XXXXXXXXXXXXXXXXXXXXXXXX"
CHAT_ID="-4121164297"
TG_API_URL="https://api.telegram.org/bot$TG_BOT_TOKEN/sendMessage"
MESSAGE_TEXT="All%20checks%20have%20been%20done!%20Please%20start%20project%20deployment!%0AURL:%20$CI_PROJECT_URL/pipelines/$CI_PIPELINE_ID/"
MESSAGE_URL_POST="chat_id=$CHAT_ID&disable_web_page_preview=1&text=$MESSAGE_TEXT"

# curl - утилита передачи данных с помощью URL
# -s - silent
# --max-time - время выполнения команды
# -d - режим отправки запроса POST на URL
echo "$MESSAGE_TEXT\n"
curl -s --max-time $UPTIME -d $MESSAGE_URL_POST $TG_API_URL > /dev/null
