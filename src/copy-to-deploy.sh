#!/bin/bash

# Перед подключением нужно установить постоянное ssh соединение с deploy машиной от имени сервиса gitlab-runner 
# Ключ генерируем 1 раз и копируем на deploy машину
#
# sudo su gitlab-runner
# ssh-keygen -t rsa -b 2048
# ssh-copy-id willumye@192.168.1.27
# ssh willumye@192.168.1.27

scp src/cat/s21_cat willumye@192.168.1.27:~/
scp src/grep/s21_grep willumye@192.168.1.27:~/
ssh willumye@192.168.1.27 "echo "01" | sudo -S mv s21_cat s21_grep /usr/local/bin"