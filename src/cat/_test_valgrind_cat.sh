#!/bin/bash

SUCCESS=0
FAIL=0
COUNTER=0
RESULT=0
DIFF_RES=""
TEST_DIR="TESTS"

declare -a tests=(
"VAR $TEST_DIR/test_case_cat.txt"
"VAR $TEST_DIR/no_file.txt"
)

declare -a extra=(
"-s $TEST_DIR/test_1_cat.txt"
"-b -e -n -s -t -v $TEST_DIR/test_1_cat.txt"
"-t $TEST_DIR/test_3_cat.txt"
"-n $TEST_DIR/test_2_cat.txt"
"$TEST_DIR/no_file.txt"
"-n -b $TEST_DIR/test_1_cat.txt"
"-s -n -e $TEST_DIR/test_4_cat.txt"
"$TEST_DIR/test_1_cat.txt -n"
"-n $TEST_DIR/test_1_cat.txt"
"-n $TEST_DIR/test_1_cat.txt $TEST_DIR/test_2_cat.txt"
"-v $TEST_DIR/test_5_cat.txt"
)

testing()
{
    t=$(echo $@ | sed "s/VAR/$var/")
    valgrind --vgdb=no --leak-check=full --show-leak-kinds=all --track-origins=yes --verbose --log-file=LOGS/RESULT_VALGRIND.LOG ./s21_cat $t > LOGS/RESULT_CAT.LOG
    leak=$(grep ERROR LOGS/RESULT_VALGRIND.LOG)
    (( COUNTER++ ))
    if [[ $leak == *"ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)"* ]]
    then
      (( SUCCESS++ ))
        echo -e "\033[32m$COUNTER - Success\033[0m cat $t"
    else
      (( FAIL++ ))
        echo -e "\033[31m$COUNTER - Fail\033[0m cat $t"
#        echo "$leak"
    fi
    rm -rf LOGS/RESULT_CAT.LOG
    rm -rf LOGS/RESULT_VALGRIND.LOG
}

# специфические тесты
for i in "${extra[@]}"
do
    var="-"
    testing $i
done

# 1 параметр
for var1 in b e n s t v
do
    for i in "${tests[@]}"
    do
        var="-$var1"
        testing $i
    done
done

# 2 параметра
for var1 in b e n s t v
do
    for var2 in b e n s t v
    do
        if [ $var1 != $var2 ]
        then
            for i in "${tests[@]}"
            do
                var="-$var1 -$var2"
                testing $i
            done
        fi
    done
done

# 3 параметра
for var1 in b e n s t v
do
    for var2 in b e n s t v
    do
        for var3 in b e n s t v
        do
            if [ $var1 != $var2 ] && [ $var2 != $var3 ] && [ $var1 != $var3 ]
            then
                for i in "${tests[@]}"
                do
                    var="-$var1 -$var2 -$var3"
                    testing $i
                done
            fi
        done
    done
done

# 4 параметра
for var1 in b e n s t v
do
    for var2 in b e n s t v
    do
        for var3 in b e n s t v
        do
            for var4 in b e n s t v
            do
                if [ $var1 != $var2 ] && [ $var2 != $var3 ] \
                && [ $var1 != $var3 ] && [ $var1 != $var4 ] \
                && [ $var2 != $var4 ] && [ $var3 != $var4 ]
                then
                    for i in "${tests[@]}"
                    do
                        var="-$var1 -$var2 -$var3 -$var4"
                        testing $i
                    done
                fi
            done
        done
    done
done

echo -e "\033[31mFAIL: $FAIL\033[0m"
echo -e "\033[32mSUCCESS: $SUCCESS\033[0m"
echo -e "ALL: $COUNTER"
