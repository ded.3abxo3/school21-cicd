#!/bin/bash
SUCCESS=0
FAIL=0
COUNTER=0
DIFF=""
LOGDIR="TESTS/logs"
TESTDIR="TESTS/test_files"

s21_command=(
    "s21_grep"
    )

sys_command=(
    "grep"
    )

flags=(
    "i"
    "v"
    "c"
    "l"
    "n"
    "h"
    "o"
    "s"
)

tests=(
"for $TESTDIR/test_1_grep.txt $TESTDIR/test_2_grep.txt $TESTDIR/test_3_grep.txt FLAGS"
"for $TESTDIR/test_1_grep.txt FLAGS"
"-e for -e int $TESTDIR/test_1_grep.txt $TESTDIR/test_2_grep.txt $TESTDIR/test_3_grep.txt FLAGS"
"-e for -e int $TESTDIR/test_1_grep.txt FLAGS"
"-e print -e int $TESTDIR/test_1_grep.txt FLAGS -f $TESTDIR/test_ptrn_grep.txt"
"-e while -e for $TESTDIR/test_1_grep.txt $TESTDIR/test_2_grep.txt $TESTDIR/test_3_grep.txt FLAGS -f $TESTDIR/test_ptrn_grep.txt"
)

manual=(
"-n for $TESTDIR/test_1_grep.txt $TESTDIR/test_2_grep.txt"
"-n for $TESTDIR/test_1_grep.txt"
"-n -e ^\} $TESTDIR/test_1_grep.txt"
"-c -e \/ $TESTDIR/test_1_grep.txt"
"-ce ^int $TESTDIR/test_1_grep.txt $TESTDIR/test_2_grep.txt"
"-e ^int $TESTDIR/test_1_grep.txt"
"-nivh = $TESTDIR/test_1_grep.txt $TESTDIR/test_2_grep.txt"
"-ie INT $TESTDIR/test_5_grep.txt"
"-echar $TESTDIR/test_1_grep.txt $TESTDIR/test_2_grep.txt"
"-ne = -e out $TESTDIR/test_5_grep.txt"
"-iv int $TESTDIR/test_5_grep.txt"
"-in int $TESTDIR/test_5_grep.txt"
"-c -l aboba $TESTDIR/test_1_grep.txt $TESTDIR/test_5_grep.txt"
"-v $TESTDIR/test_1_grep.txt -e ank"
"-noe ')' $TESTDIR/test_5_grep.txt"
"-l for $TESTDIR/test_1_grep.txt $TESTDIR/test_2_grep.txt"
"-o -e int $TESTDIR/test_4_grep.txt"
"-e = -e out $TESTDIR/test_5_grep.txt"
"-noe ing -e as -e the -e not -e is $TESTDIR/test_6_grep.txt"
"-e ing -e as -e the -e not -e is $TESTDIR/test_6_grep.txt"
"-l for no_file.txt $TESTDIR/test_2_grep.txt"
"-e int -si no_file.txt s21_grep.c no_file2.txt s21_grep.h"
"-si s21_grep.c -f no_pattern.txt"
"-f $TESTDIR/test_ptrn_grep.txt $TESTDIR/test_5_grep.txt"
)

run_test() {
    param=$(echo "$@" | sed "s/FLAGS/$var/")
    ./"${s21_command[@]}" $param &> $LOGDIR/"${s21_command[@]}".log
    "${sys_command[@]}" $param &> $LOGDIR/"${sys_command[@]}".log
    DIFF="$(diff -s $LOGDIR/"${s21_command[@]}".log $LOGDIR/"${sys_command[@]}".log)"
    let "COUNTER++"
    if [ "$DIFF" == "Files "$LOGDIR/${s21_command[@]}".log and "$LOGDIR/${sys_command[@]}".log are identical" ]
    then
        let "SUCCESS++"
        echo -e "\033[92m$COUNTER - Success\033[0m $param"
    else
        let "FAIL++"
        echo -e "\033[91m$COUNTER - Fail\033[0m $param"
    fi
    rm -f $LOGDIR/"${s21_command[@]}".log $LOGDIR/"${sys_command[@]}".log
}

echo -e "\033[91mВНИМАНИЕ! Системный grep НА маках КРИВУЩЩИЙ. Некорректная обработка совместных флагов -c -l\033[0m"
echo -e "\033[96m##################################\033[0m"
echo -e "\033[96mMANUAL TESTS (Извращался, как мог)\033[0m"
echo -e "\033[96m##################################\033[0m"
printf "\n"

for i in "${manual[@]}"
do
    var="-"
    run_test "$i"
done

printf "\n"
echo -e "\033[96m=======================\033[0m"
echo -e "\033[96mAUTOTEST 1 PARAMETER\033[0m"
echo -e "\033[96m=======================\033[0m"
printf "\n"

for var1 in "${flags[@]}"
do
    for i in "${tests[@]}"
    do
        var="-$var1"
        run_test "$i"
    done
done
printf "\n"
echo -e "\033[96m=======================\033[0m"
echo -e "\033[96mAUTOTEST 2 PARAMETERS\033[0m"
echo -e "\033[96m=======================\033[0m"
printf "\n"

for var1 in "${flags[@]}"
do
    for var2 in "${flags[@]}"
    do
        if [ $var1 != $var2 ]
        then
            for i in "${tests[@]}"
            do
                var="-$var1 -$var2"
                run_test "$i"
            done
        fi
    done
done
printf "\n"

echo -e "\033[91mFAIL: $FAIL\033[0m"
echo -e "\033[92mSUCCESS: $SUCCESS\033[0m"
echo -e "\033[96mALL: $COUNTER\033[0m"
printf "\n"
##############################
